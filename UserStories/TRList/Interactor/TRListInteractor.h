//
//  TRListInteractor.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "TRListInteractorInput.h"

// Protocols
@protocol TRListInteractorOutput;

@interface TRListInteractor : NSObject <TRListInteractorInput>

@property (nonatomic, weak) id<TRListInteractorOutput> output;

@end
