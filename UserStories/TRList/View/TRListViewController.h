//
//  TRListViewController.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "TRListViewInput.h"

// Protocols
@protocol TRListViewOutput;

@interface TRListViewController : UIViewController <TRListViewInput>

@property (nonatomic, strong) id<TRListViewOutput> output;

@end
