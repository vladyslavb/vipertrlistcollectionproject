//
//  TRListViewInput.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol TRListViewInput <NSObject>

/**
 @author Vladyslav Bedro

 Method for setup initial state of view
 */
- (void) setupInitialState;

@end
