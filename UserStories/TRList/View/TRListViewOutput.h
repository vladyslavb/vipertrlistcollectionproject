//
//  TRListViewOutput.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol TRListViewOutput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method which inform presenter, that view is ready for a work
 */
- (void) didTriggerViewReadyEvent;

@end
