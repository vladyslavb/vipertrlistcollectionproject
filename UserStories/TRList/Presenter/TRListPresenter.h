//
//  TRListPresenter.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "TRListViewOutput.h"
#import "TRListInteractorOutput.h"
#import "TRListModuleInput.h"

// Protocols
@protocol TRListViewInput;
@protocol TRListInteractorInput;
@protocol TRListRouterInput;

@interface TRListPresenter : NSObject <TRListModuleInput, TRListViewOutput, TRListInteractorOutput>

@property (nonatomic, weak) id<TRListViewInput> view;
@property (nonatomic, strong) id<TRListInteractorInput> interactor;
@property (nonatomic, strong) id<TRListRouterInput> router;

@end
