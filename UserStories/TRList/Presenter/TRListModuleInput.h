//
//  TRListModuleInput.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;
#import <ViperMcFlurry/ViperMcFlurry.h>

// Protocols
@protocol TRListModuleInput <RamblerViperModuleInput>

/**
 @author Vladyslav Bedro

 Method initialized start configuration of current module
 */
- (void) configureModule;

@end
