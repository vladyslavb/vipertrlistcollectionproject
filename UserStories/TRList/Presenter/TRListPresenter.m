//
//  TRListPresenter.m
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "TRListPresenter.h"

// Classes
#import "TRListViewInput.h"
#import "TRListInteractorInput.h"
#import "TRListRouterInput.h"

@implementation TRListPresenter


#pragma mark - Methods TRListModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods TRListViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods TRListInteractorOutput -


@end
