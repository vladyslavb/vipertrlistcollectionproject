//
//  TRListAssembly.m
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "TRListAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "TRListViewController.h"
#import "TRListInteractor.h"
#import "TRListPresenter.h"
#import "TRListRouter.h"


@implementation TRListAssembly


#pragma mark - Initialization methods -

- (TRListViewController*) viewTRList 
{
    return [TyphoonDefinition withClass: [TRListViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterTRList]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterTRList]];
                                                    
                          }];
}

- (TRListInteractor*) interactorTRList 
{
    return [TyphoonDefinition withClass: [TRListInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterTRList]];
                                                    
                          }];
}

- (TRListPresenter*) presenterTRList
{
    return [TyphoonDefinition withClass: [TRListPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewTRList]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorTRList]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerTRList]];
                                                    
                          }];
}

- (TRListRouter*) routerTRList
{
    return [TyphoonDefinition withClass: [TRListRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewTRList]];
                                                    
                          }];
}

@end
