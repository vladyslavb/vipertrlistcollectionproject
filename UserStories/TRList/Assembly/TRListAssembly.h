//
//  TRListAssembly.h
//  ViperTRListCollectionProject
//
//  Created by Vladyslav Bedro on 25/12/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vladyslav Bedro

 TRList module
 */
@interface TRListAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
